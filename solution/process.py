import glob 
import json
from typing import List
import pandas as pd
from pprint import pprint

def load(json_path: List[str]): 
    json_files = [json.loads(open(path).read()) for path in json_path]
    return pd.DataFrame(json_files)

def reload(): 
    data_card = load(glob.glob('./data/cards/*.json'))
    data_savings_account = load(glob.glob('./data/savings_accounts/*.json'))
    data_accounts = load(glob.glob('./data/accounts/*.json'))
    return data_card, data_savings_account, data_accounts

def print_historical(*data: pd.DataFrame): 
    for d in data: 
        pprint(d.sort_values('ts').reset_index(drop=True).to_records('dict'))

def print_denormalized(*data: pd.DataFrame): 
    df = pd.concat(data).reset_index(drop=True)
    df_u = df[df['op']=='u']
    df_c = df[df['op']=='c']
    temp = {}
    for item in df_c.id.unique(): 
        temp[item] = []
    for item in df_u[['id', 'set', 'ts']].to_dict('records'): 
        item['set']['ts'] = item['ts']
        temp[item['id']].append(item['set'])
    df_c = df_c.to_dict('records')
    for item1, (id_, item2) in zip(df_c, temp.items()): 
        if item1['id'] == id_: 
            item1['set']=item2
    pprint(df_c)

def run(): 
    data_card, data_savings_account, data_accounts = reload()
    print_historical(data_card, data_savings_account, data_accounts)
    print_denormalized(data_card, data_savings_account, data_accounts)
